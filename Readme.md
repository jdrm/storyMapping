# Story Mapping

Es un script de generación de gráficos de mapas de historia pensado
para plasmar en un .png una historia diseñada en un fichero de texto que usa como entrada

Para ejecutarlo:

python src/sm.py examples/ejemplo.txt

## Creación del fichero de entrada de datos

Está compuesto de líneas, cada una corresponde con una etiqueta. Cada línea lleva un número de campos separados por la barra (|)

En la primera posición va el tipo de etiqueta que se quiere pintar, pudiendo ser:
> persona/requisito/cosa generica
· descripción de una entrega
- tarea
+ sub-tarea, actividad
* criterio
$ Límite de una entrega

Hay que tener en cuenta las siguientes reglas:

* Todas las tarjetas de persona/requisito van seguidas al principio
* A continuación van todas las descripciones de las entregas.
* A continuación va una tarea, seguida de sus subtareas
* Cada subtarea va seguida de sus criterios. De momento el diseño está pensado para un máximo de 6 criterios por cada subtarea/actividad
* Si hay criterios de una misma subtarea en varias entregas se separan con una línea $ y el número de la siguiente entrega

Ejemplo:

>| Persona 1 | Descripcion |
>| Persona 2 | Descripcion |
·| Entrega 1 |
·| Entrega 2 |
-| Tarea 1 | Descripcion de la tarea |
+| Subtarea 1 | Subtarea 1 de la Tarea 1 |
*| Criterio 1 |
$| 2 |
*| Criterio 2 |
*| Criterio 3 |
+| Subtarea 2 | de la Tarea 1 |
$| 2 |
*| Criterio 4 |
-|Tarea 2 | Tarea 2|
+| subtarea 3 | de la Tarea 2 |
*| Criterio 5 |
