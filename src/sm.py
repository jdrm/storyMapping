#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import tkinter

def daExtremos(posX, posY):
	newX = posX*50
	newY = posY*50
	return(newX, newY, newX+45, newY+45)

def daCentro(posX, posY):
	newX = (posX*50)+22
	newY = (posY*50)+22
	return(newX, newY)
	
colorAzul = "#A89FD2"

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--file", help="Nombre de archivo a procesar")
args = parser.parse_args()

# Creo la ventana
ventana = tkinter.Tk()
areaDeDibujo = tkinter.Canvas( ventana )

# Hacer visibles
areaDeDibujo.pack()

# Tendré que guardar la posición en la que he pintado el último objeto
# las posiciones X e Y van a ser posiciones en una cuadrícula
posicionX = 0 #horizontal
posicionY = 0 #vertical

# Abro el fichero de entrada pasado por parámetro

fichero = open(args.file)

# Por cada línea leo usando de separador de campo |
# el primer camppo me dice si es 
#	> persona/requisito/cosa generica
#	· descripción de una entrega
#	- tarea
#	+ sub-tarea, actividad
#	* criterio
# 	$ Límite de una entrega
# 
# El segundo campo me da el titular
# El tercero el texto que sigue
#
for linea in fichero:
	listaOpciones = linea.split('|')
	if listaOpciones[0] == '>':
		# movemos, en la primera fila, a la siguiente columna
		posicionX = posicionX + 1

		print("Rectangulo " + str(daExtremos(posicionX, posicionY)))
		rectangulo = areaDeDibujo.create_rectangle( daExtremos(posicionX, posicionY), fill=colorAzul)
		print("Texto " + str(daCentro(posicionX, posicionY)))
		texto = areaDeDibujo.create_text( daCentro(posicionX, posicionY), text=listaOpciones[1])
	elif listaOpciones[0] == '·':
		# reseteamos posicionY para volver a la parte superior y sumamos 100 a posicionX para pasar a la siguiente columNA
		posicionX = 0
		posicionY = 1
		print(listaOpciones)
		
print("Fin")
# Por cada contenido voy dibujando, teniendo en cuenta que primero pinto todos los >
# y que debajo de cada - van los + y debajo de cada + los *. La siguiente tarea - 
# implica un cambio de columna. Tendré que controlar el ancho que voy pintando

ventana.mainloop()
